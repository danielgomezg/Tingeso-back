FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
ADD target/Tingeso.jar Tingeso.jar
EXPOSE 5000
ENTRYPOINT ["java", "-jar", "Tingeso.jar"]
